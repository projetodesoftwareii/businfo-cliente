<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<title>BusInfo - Enviar Geolocalização</title>
</head>
<script type="text/javascript" src="jquery-2.1.1.min.js"></script>
<body>
<p>* Para fins de teste, use o Google Chrome, que salva a opção de permitir acesso a geolocalização</p>
<div id="saida"></div>
<div id="timer">Próxima atualização em <span id="tempo">60</span> seg</div>
<?php $usuario = isset($_GET['usuario']) ? addslashes($_GET['usuario']) : 'onibus1'; ?>
<script type="text/javascript">
	// Executa a primeira vez
	getGeolocalizacao();
	// Atualiza geolocalizacao a cada 60 segundos
	setInterval(function () { getGeolocalizacao(); }, 60000);
	setInterval(function () { atualizarTimer(); }, 1000);
	function getGeolocalizacao() {
	    if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(enviarGeolocalizacao);
	    } else { 
	        $('#saida').html("Navegador não suporta.");
	    }
	}

	function enviarGeolocalizacao(localizacao) {
		var request = $.ajax({
			type: "POST",
			url: "ajax_enviar_geolocalizacao.php",
			//url: "http://localhost/index.php?&callback=",
			data: {usuario: '<?php echo $usuario; ?>', senha: 'admin123', latitude: localizacao.coords.latitude, longitude: localizacao.coords.longitude},
			dataType: 'jsonp'
		});

		request.always(function() {
			$('#saida').html(request.responseText);
		});
	}
	function atualizarTimer() {
		var seg = $('#timer #tempo').html();
		if (seg - 1 <= 0) {
			seg = 60;
		}
		$('#timer #tempo').html(seg - 1);
	}
</script>
</body>
</html>