<?php
	require_once('config.php');
	// Precisa de curl instalado
	extract($_POST);

	//set POST variables
	$url = HOST_SERVIDOR . '/tempoPainel/index';
	$fields = array(
		'usuario' 	=> urlencode($usuario),
		'senha' 	=> urlencode($senha),	
	);

	//url-ify the data for the POST
	$fields_string = '';
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');

	//open connection
	$ch = curl_init();
	//set the url, number of POST vars, POST data,ativar retorno
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	//execute post
	$result = curl_exec($ch);
	//close connection
	curl_close($ch);
	// exibe o retorno
	echo $result;
?>