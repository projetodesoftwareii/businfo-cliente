<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="UTF-8">
	<title>BusInfo - Tempo Painel</title>
	<style>
		.text { cursor:pointer;overflow:hidden;font:40px/1 monospace; }
		.text span { display:block;float:left;background:#444;color:#fff;margin-right:1px; }
	</style>
</head>
<script type="text/javascript" src="jquery-2.1.1.min.js"></script>
<body>

<?php $usuario = isset($_GET['usuario']) ? addslashes($_GET['usuario']) : 'painel1'; ?>  

<div id="saida">
	<div id="nomeParada"></div>
	<div id="tempo"></div>
	<div id="timer">Próxima atualização em <span id="tempo">0</span> seg</div>
</div>

<script type="text/javascript">
	// Executa a primeira vez
	getTempoPainel();
	// Atualiza geolocalizacao a cada 60 segundos
	setInterval(function () { getTempoPainel(); }, 60000);
	setInterval(function () { atualizarTimer(); }, 1000);
	function getTempoPainel() {
		var request = $.ajax({
			type: "POST",
			url: "./ajax_tempo_painel.php",
			data: {usuario: '<?php echo $usuario; ?>', senha: 'admin123'},
			dataType: 'jsonp'
		});

		request.fail(function(){
			$('#tempo').html('Ocorreu um erro na conexão com o servidor.');
		});
		request.always(function() {
			var retorno = JSON.parse(request.responseText);
			if (retorno.status.codigo == 1) {
				$('#nomeParada').html(retorno.parada.nome);
				$('#tempo').html('<table></table>');
				//$('#tempo').html('');
				$.each(retorno.parada.linhas, function(i, valor){
					$('#tempo table').append("<tr><td>" + valor.linha + "</td><td>" + valor.tempo + "</td></tr>");
					//$('#tempo').append("<ul class='text'><li>" + valor.linha + "  " + valor.tempo + "</li></ul>")
					//alert(valor.tempo + "min");
				});
				$('.text').ticker();
			} else if (retorno.status.codigo == 0) {
				$('#tempo').html(retorno.status.mensagem);
			}
		});
		$('#timer #tempo').html('60');
	}
	function atualizarTimer() {
		var seg = $('#timer #tempo').html();
		if (seg - 1 <= 0) {
			seg = 60;
		}
		$('#timer #tempo').html(seg - 1);
	}

</script>
</body>
</html>